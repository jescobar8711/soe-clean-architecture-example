--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.0

-- Started on 2020-03-04 00:45:25

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 162633)
-- Name: categoria; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.categoria (
    id integer NOT NULL,
    nro character varying NOT NULL,
    nombre character varying NOT NULL,
    descripcion character varying
);


ALTER TABLE public.categoria OWNER TO zgdtzszg;

--
-- TOC entry 202 (class 1259 OID 162631)
-- Name: categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.categoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 202
-- Name: categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.categoria_id_seq OWNED BY public.categoria.id;


--
-- TOC entry 211 (class 1259 OID 162684)
-- Name: cliente; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nro character varying NOT NULL,
    nombres character varying NOT NULL,
    apellidos character varying NOT NULL,
    direccion character varying,
    telefono character varying,
    correo character varying,
    fecha date
);


ALTER TABLE public.cliente OWNER TO zgdtzszg;

--
-- TOC entry 210 (class 1259 OID 162682)
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 210
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;


--
-- TOC entry 214 (class 1259 OID 162697)
-- Name: factura; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.factura (
    id integer NOT NULL,
    nro integer NOT NULL,
    fecha date NOT NULL,
    cliente_id integer NOT NULL,
    total numeric
);


ALTER TABLE public.factura OWNER TO zgdtzszg;

--
-- TOC entry 216 (class 1259 OID 162711)
-- Name: factura_detalle; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.factura_detalle (
    id integer NOT NULL,
    factura_id integer NOT NULL,
    producto_id integer NOT NULL,
    cantidad integer NOT NULL,
    precio numeric NOT NULL,
    total numeric NOT NULL
);


ALTER TABLE public.factura_detalle OWNER TO zgdtzszg;

--
-- TOC entry 218 (class 1259 OID 162722)
-- Name: modo_pago; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.modo_pago (
    id integer NOT NULL,
    nro character varying NOT NULL,
    descripcion character varying NOT NULL
);


ALTER TABLE public.modo_pago OWNER TO zgdtzszg;

--
-- TOC entry 217 (class 1259 OID 162720)
-- Name: modo_pago_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.modo_pago_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modo_pago_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 217
-- Name: modo_pago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.modo_pago_id_seq OWNED BY public.modo_pago.id;


--
-- TOC entry 213 (class 1259 OID 162695)
-- Name: orden_factura_nro_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.orden_factura_nro_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orden_factura_nro_seq OWNER TO zgdtzszg;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 213
-- Name: orden_factura_nro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.orden_factura_nro_seq OWNED BY public.factura.nro;


--
-- TOC entry 215 (class 1259 OID 162709)
-- Name: orden_venta_detalle_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.orden_venta_detalle_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orden_venta_detalle_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 215
-- Name: orden_venta_detalle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.orden_venta_detalle_id_seq OWNED BY public.factura_detalle.id;


--
-- TOC entry 212 (class 1259 OID 162693)
-- Name: orden_venta_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.orden_venta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orden_venta_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 212
-- Name: orden_venta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.orden_venta_id_seq OWNED BY public.factura.id;


--
-- TOC entry 221 (class 1259 OID 162735)
-- Name: pago; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.pago (
    id integer NOT NULL,
    nro integer NOT NULL,
    total numeric NOT NULL,
    fecha date NOT NULL,
    transaccion_nro character varying,
    modo_pago_id integer NOT NULL,
    factura_id integer NOT NULL
);


ALTER TABLE public.pago OWNER TO zgdtzszg;

--
-- TOC entry 219 (class 1259 OID 162731)
-- Name: pago_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.pago_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pago_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 219
-- Name: pago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.pago_id_seq OWNED BY public.pago.id;


--
-- TOC entry 220 (class 1259 OID 162733)
-- Name: pago_nro_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.pago_nro_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pago_nro_seq OWNER TO zgdtzszg;

--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 220
-- Name: pago_nro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.pago_nro_seq OWNED BY public.pago.nro;


--
-- TOC entry 205 (class 1259 OID 162644)
-- Name: producto; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.producto (
    id integer NOT NULL,
    nro character varying NOT NULL,
    nombre character varying NOT NULL,
    stock integer NOT NULL,
    categoria_id integer NOT NULL,
    precio numeric
);


ALTER TABLE public.producto OWNER TO zgdtzszg;

--
-- TOC entry 204 (class 1259 OID 162642)
-- Name: producto_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.producto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.producto_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 204
-- Name: producto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.producto_id_seq OWNED BY public.producto.id;


--
-- TOC entry 208 (class 1259 OID 162665)
-- Name: user; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    login character varying NOT NULL,
    psw character varying NOT NULL,
    status bigint DEFAULT 1 NOT NULL,
    level_id integer DEFAULT 0 NOT NULL,
    typee integer DEFAULT 0 NOT NULL,
    parent_id integer,
    mail character varying,
    activated integer DEFAULT 1
);


ALTER TABLE public."user" OWNER TO zgdtzszg;

--
-- TOC entry 207 (class 1259 OID 162663)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: zgdtzszg
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO zgdtzszg;

--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zgdtzszg
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 209 (class 1259 OID 162677)
-- Name: userlevelpermissions; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.userlevelpermissions (
    userlevelid integer NOT NULL,
    tablename character varying(255) NOT NULL,
    permission integer NOT NULL
);


ALTER TABLE public.userlevelpermissions OWNER TO zgdtzszg;

--
-- TOC entry 206 (class 1259 OID 162658)
-- Name: userlevels; Type: TABLE; Schema: public; Owner: zgdtzszg
--

CREATE TABLE public.userlevels (
    userlevelid integer NOT NULL,
    userlevelname character varying(255) NOT NULL
);


ALTER TABLE public.userlevels OWNER TO zgdtzszg;

--
-- TOC entry 2749 (class 2604 OID 162636)
-- Name: categoria id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.categoria ALTER COLUMN id SET DEFAULT nextval('public.categoria_id_seq'::regclass);


--
-- TOC entry 2756 (class 2604 OID 162687)
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.cliente_id_seq'::regclass);


--
-- TOC entry 2757 (class 2604 OID 162700)
-- Name: factura id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura ALTER COLUMN id SET DEFAULT nextval('public.orden_venta_id_seq'::regclass);


--
-- TOC entry 2758 (class 2604 OID 162701)
-- Name: factura nro; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura ALTER COLUMN nro SET DEFAULT nextval('public.orden_factura_nro_seq'::regclass);


--
-- TOC entry 2759 (class 2604 OID 162714)
-- Name: factura_detalle id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura_detalle ALTER COLUMN id SET DEFAULT nextval('public.orden_venta_detalle_id_seq'::regclass);


--
-- TOC entry 2760 (class 2604 OID 162725)
-- Name: modo_pago id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.modo_pago ALTER COLUMN id SET DEFAULT nextval('public.modo_pago_id_seq'::regclass);


--
-- TOC entry 2761 (class 2604 OID 162738)
-- Name: pago id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.pago ALTER COLUMN id SET DEFAULT nextval('public.pago_id_seq'::regclass);


--
-- TOC entry 2762 (class 2604 OID 162739)
-- Name: pago nro; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.pago ALTER COLUMN nro SET DEFAULT nextval('public.pago_nro_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 162647)
-- Name: producto id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.producto ALTER COLUMN id SET DEFAULT nextval('public.producto_id_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 162668)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 2914 (class 0 OID 162633)
-- Dependencies: 203
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.categoria (id, nro, nombre, descripcion) FROM stdin;
1	tv	Televisores y equipos de sonido	\N
2	pc	Portátiles y Computadoras	\N
3	el	Electrónica e Informática	\N
\.


--
-- TOC entry 2922 (class 0 OID 162684)
-- Dependencies: 211
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.cliente (id, nro, nombres, apellidos, direccion, telefono, correo, fecha) FROM stdin;
1	6392312	jesus	esobar ovando	\N	\N	\N	\N
\.


--
-- TOC entry 2925 (class 0 OID 162697)
-- Dependencies: 214
-- Data for Name: factura; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.factura (id, nro, fecha, cliente_id, total) FROM stdin;
1	1	2020-02-27	1	\N
2	1	2020-03-02	1	12
3	1	2020-03-02	1	12
4	1	2020-03-03	1	12
5	1	2020-03-03	1	12
6	1	2020-03-03	1	12
7	1	2020-03-03	1	12
8	1	2020-03-03	1	12
9	1	2020-03-03	1	12
10	1	2020-03-03	1	12
11	1	2020-03-03	1	12
12	1	2020-03-03	1	12
13	1	2020-03-03	1	12
14	1	2020-03-04	1	12
15	1	2020-03-04	1	12
16	21	2020-03-04	1	12
17	22	2020-03-04	1	12
18	23	2020-03-04	1	12
\.


--
-- TOC entry 2927 (class 0 OID 162711)
-- Dependencies: 216
-- Data for Name: factura_detalle; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.factura_detalle (id, factura_id, producto_id, cantidad, precio, total) FROM stdin;
1	1	2	2	250	500
2	1	1	1	100	100
3	12	1	2	300	300
4	12	2	3	21000	21000
5	13	1	2	300	300
6	13	2	3	21000	21000
7	14	1	2	300	300
8	14	2	3	21000	21000
9	15	1	2	300	300
10	15	2	3	21000	21000
11	16	1	2	300	300
12	16	2	3	21000	21000
13	17	1	2	300	300
14	17	2	3	21000	21000
15	18	1	2	300	300
16	18	2	3	21000	21000
\.


--
-- TOC entry 2929 (class 0 OID 162722)
-- Dependencies: 218
-- Data for Name: modo_pago; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.modo_pago (id, nro, descripcion) FROM stdin;
1	efectivo	pago en efectivo
2	tarjeta	pago mediante tarjeta de credido/ devito
\.


--
-- TOC entry 2932 (class 0 OID 162735)
-- Dependencies: 221
-- Data for Name: pago; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.pago (id, nro, total, fecha, transaccion_nro, modo_pago_id, factura_id) FROM stdin;
1	0	200	2020-02-27	\N	1	1
2	1	100	2020-02-02	\N	2	1
\.


--
-- TOC entry 2916 (class 0 OID 162644)
-- Dependencies: 205
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.producto (id, nro, nombre, stock, categoria_id, precio) FROM stdin;
2	PR-01811	TV 32″ SURE CON BARRA DE SONIDO INTEGRADA Y SINTONIZADOR DIGITAL	200	1	250
1	17798	TV 16″ SURE LED HD	20	1	1400
3	PR-01382	SMART TV 55″FHD SURE CON SINTONIZADOR DIGITAL	150	1	100
4	PR-02106	LAPTOP ACER NITRO5 CORE i5	20	2	700
5	PR-02106	EQUIPO SURE CORE I3-8100	20	2	500
\.


--
-- TOC entry 2919 (class 0 OID 162665)
-- Dependencies: 208
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public."user" (id, login, psw, status, level_id, typee, parent_id, mail, activated) FROM stdin;
1	jescobar	jescobar	1	0	0	\N	jescobar8711@gmail.com	\N
2	jes	jes	1	0	0	\N	jes	1
3	lorena	123456	1	0	0	\N	jescobar@hotmail.com	1
\.


--
-- TOC entry 2920 (class 0 OID 162677)
-- Dependencies: 209
-- Data for Name: userlevelpermissions; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.userlevelpermissions (userlevelid, tablename, permission) FROM stdin;
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}orden_venta_detalle	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}producto	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}cliente	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}pago	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}orden_venta	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}modo_pago	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}factura	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}user	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}categoria	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}userlevelpermissions	0
-2	{226DBEB9-38FD-474A-86A9-ACE804C23674}userlevels	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}pago	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}orden_venta	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}modo_pago	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}factura	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}orden_venta_detalle	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}producto	360
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}cliente	367
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}user	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}categoria	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}userlevelpermissions	0
0	{226DBEB9-38FD-474A-86A9-ACE804C23674}userlevels	0
\.


--
-- TOC entry 2917 (class 0 OID 162658)
-- Dependencies: 206
-- Data for Name: userlevels; Type: TABLE DATA; Schema: public; Owner: zgdtzszg
--

COPY public.userlevels (userlevelid, userlevelname) FROM stdin;
-2	Anonymous
-1	Administrator
0	Default
\.


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 202
-- Name: categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.categoria_id_seq', 3, true);


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 210
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.cliente_id_seq', 1, true);


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 217
-- Name: modo_pago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.modo_pago_id_seq', 2, true);


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 213
-- Name: orden_factura_nro_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.orden_factura_nro_seq', 23, true);


--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 215
-- Name: orden_venta_detalle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.orden_venta_detalle_id_seq', 16, true);


--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 212
-- Name: orden_venta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.orden_venta_id_seq', 18, true);


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 219
-- Name: pago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.pago_id_seq', 2, true);


--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 220
-- Name: pago_nro_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.pago_nro_seq', 1, false);


--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 204
-- Name: producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.producto_id_seq', 5, true);


--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zgdtzszg
--

SELECT pg_catalog.setval('public.user_id_seq', 3, true);


--
-- TOC entry 2764 (class 2606 OID 162641)
-- Name: categoria categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (id);


--
-- TOC entry 2774 (class 2606 OID 162692)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2780 (class 2606 OID 162730)
-- Name: modo_pago modo_pago_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.modo_pago
    ADD CONSTRAINT modo_pago_pkey PRIMARY KEY (id);


--
-- TOC entry 2778 (class 2606 OID 162719)
-- Name: factura_detalle orden_venta_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura_detalle
    ADD CONSTRAINT orden_venta_detalle_pkey PRIMARY KEY (id);


--
-- TOC entry 2776 (class 2606 OID 162703)
-- Name: factura orden_venta_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura
    ADD CONSTRAINT orden_venta_pkey PRIMARY KEY (id);


--
-- TOC entry 2782 (class 2606 OID 162744)
-- Name: pago pagos_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.pago
    ADD CONSTRAINT pagos_pkey PRIMARY KEY (id);


--
-- TOC entry 2772 (class 2606 OID 162681)
-- Name: userlevelpermissions pkuserlevelpermissions; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.userlevelpermissions
    ADD CONSTRAINT pkuserlevelpermissions PRIMARY KEY (userlevelid, tablename);


--
-- TOC entry 2768 (class 2606 OID 162662)
-- Name: userlevels pkuserlevels; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.userlevels
    ADD CONSTRAINT pkuserlevels PRIMARY KEY (userlevelid);


--
-- TOC entry 2766 (class 2606 OID 162652)
-- Name: producto producto_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- TOC entry 2770 (class 2606 OID 162676)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2784 (class 2606 OID 162704)
-- Name: factura orden_venta_cliente_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.factura
    ADD CONSTRAINT orden_venta_cliente_id_fkey FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);


--
-- TOC entry 2785 (class 2606 OID 162745)
-- Name: pago pago_modo_pago_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.pago
    ADD CONSTRAINT pago_modo_pago_id_fkey FOREIGN KEY (modo_pago_id) REFERENCES public.modo_pago(id);


--
-- TOC entry 2786 (class 2606 OID 162750)
-- Name: pago pago_orden_venta_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.pago
    ADD CONSTRAINT pago_orden_venta_id_fkey FOREIGN KEY (factura_id) REFERENCES public.factura(id);


--
-- TOC entry 2783 (class 2606 OID 162653)
-- Name: producto producto_categoria_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zgdtzszg
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_categoria_id_fkey FOREIGN KEY (categoria_id) REFERENCES public.categoria(id);


-- Completed on 2020-03-04 00:45:25

--
-- PostgreSQL database dump complete
--

