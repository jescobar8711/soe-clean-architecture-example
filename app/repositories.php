<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Domain\Factura\FacturaRepository;
use App\Domain\Factura\FacturaDetalleRepository;
use App\Infrastructure\Persistence\User\InDbUserRepository;
use App\Infrastructure\Persistence\Factura\InDbFacturaRepository;
use App\Infrastructure\Persistence\Factura\InDbFacturaDetalleRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InDbUserRepository::class),
        FacturaRepository::class => \DI\autowire(InDbFacturaRepository::class),
        FacturaDetalleRepository::class => \DI\autowire(InDbFacturaDetalleRepository::class),
    ]);

};
