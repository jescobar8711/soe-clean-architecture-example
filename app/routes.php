<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use App\Application\Actions\User\AddUserAction;
use App\Application\Actions\Factura\ListFacturaAction;
use App\Application\Actions\Factura\AddFacturaAction;
use App\Application\Actions\Factura\ViewFacturaAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
        $group->post('', AddUserAction::class);
    });
      $app->group('/factura', function (Group $group) {
        $group->get('', ListFacturaAction::class);
        $group->get('/{id}', ViewFacturaAction::class);
        $group->post('', AddFacturaAction::class);
    });
};
