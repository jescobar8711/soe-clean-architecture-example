<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use App\Domain\User\ValueObjects\Login;
use PDO;
class InDbUserRepository implements UserRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * InMemoryUserRepository constructor.
     *
     * @param PDO|null $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {

       return $this->connection->query("SELECT * FROM \"user\" ORDER BY id DESC")->fetchAll();
    }
    
   /**
     * {@inheritdoc}
     */
    public function findUserOfId(int $id): array
    {
        $row =$this->connection->query("SELECT * FROM \"user\" where id=".$id." ORDER BY id DESC LIMIT 1")->fetch();
       if (!isset($row)) {
            throw new UserNotFoundException();
        }
        return $row; 
    }

      /**
     * {@inheritdoc}
     */
    public function addUser( User $user): int
    {
        $row = [
            'login' => $user->getLogin()->getValue(),
            'psw' => $user->getPsw(),
            'mail' => $user->getMail(),
        ];

        $sql = 'INSERT INTO "user" (login, psw, mail) values (:login,:psw,:mail);';
        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}
