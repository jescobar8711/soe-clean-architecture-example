<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Factura;

use App\Domain\Factura\FacturaDetalle;
use App\Domain\Factura\FacturaDetalleNotFoundException;
use App\Domain\Factura\FacturaDetalleRepository;
use PDO;
class InDbFacturaDetalleRepository implements FacturaDetalleRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * InMemoryUserRepository constructor.
     *
     * @param PDO|null $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {

       return $this->connection->query("SELECT * FROM factura_detalle ORDER BY id DESC")->fetchAll();
    }
    
   /**
     * {@inheritdoc}
     */
    public function findFacturaDetalleOfId(int $id): array
    {
        $row =$this->connection->query("SELECT * FROM factura_detalle where id=".$id." ORDER BY id DESC LIMIT 1")->fetch();
       if (!isset($row)) {
            throw new FacturaNotFoundException();
        }
        return $row; 
    }
    /**
     * {@inheritdoc}
     */
    public function findAllOfFacturaId(int $facturaId): array
    {
        return $this->connection->query("SELECT * FROM factura_detalle where factura_id=".$facturaId." ORDER BY id DESC")->fetchAll();
    
    }
      /**
     * {@inheritdoc}
     */
    public function addFacturaDetalle( FacturaDetalle $facturaDetalle): int
    {
        $row = [
            'factura_id' => $facturaDetalle->getFactura(),
            'producto_id' => $facturaDetalle->getProducto(),
            'cantidad' => $facturaDetalle->getCantidad(),
            'precio' => $facturaDetalle->getPrecio(),
            'total' => $facturaDetalle->getTotal(),
            
        ];

        $sql = 'INSERT INTO factura_detalle (factura_id, producto_id, cantidad,precio,total) values (:factura_id,:producto_id,:cantidad,:precio,:total);';
        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}
