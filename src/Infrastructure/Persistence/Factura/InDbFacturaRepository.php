<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Factura;

use App\Domain\Factura\Factura;
use App\Domain\Factura\FacturaNotFoundException;
use App\Domain\Factura\FacturaRepository;
use App\Domain\Factura\ValueObjects\Nrofactura;
use App\Domain\Factura\ValueObjects\Fecha;
use App\Domain\Factura\ValueObjects\TotalFactura;
use App\Domain\Factura\ValueObjects\Cliente;
use PDO;
class InDbFacturaRepository implements FacturaRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * InMemoryUserRepository constructor.
     *
     * @param PDO|null $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {

       return $this->connection->query("SELECT * FROM factura ORDER BY id DESC")->fetchAll();
    }
    
   /**
     * {@inheritdoc}
     */
    public function findFacturaOfId(int $id): array
    {
        $row =$this->connection->query("SELECT * FROM factura where id=".$id." ORDER BY id DESC LIMIT 1")->fetch();
       if (!isset($row)) {
            throw new UserNotFoundException();
        }
        return $row; 
    }

      /**
     * {@inheritdoc}
     */
    public function addFactura( Factura $factura): int
    {
        $row = [
            'nro' => $factura->getNro()->getValue(),
            'fecha' => $factura->getFecha()->getValue(),
            'total' => $factura->getTotal()->getValue(),
            'cliente_id' => $factura->getCliente()->getValue(),
            
        ];

        $sql = 'INSERT INTO factura (nro, fecha, total,cliente_id) values (:nro,:fecha,:total,:cliente_id);';
        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    
     /**
     * {@inheritdoc}
     */
    public function nextNro(): int
    {
        $row =$this->connection->query("SELECT nextval('orden_factura_nro_seq'::regclass)")->fetch();
       if (!isset($row)) {
            throw new UserNotFoundException();
        }
        return $row['nextval']; 
    }
}
