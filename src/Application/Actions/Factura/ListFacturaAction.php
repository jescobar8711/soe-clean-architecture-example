<?php
declare(strict_types=1);

namespace App\Application\Actions\Factura;
use Psr\Http\Message\ResponseInterface as Response;

class ListFacturaAction extends FacturaAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $facturas = $this->facturaRepository->findAll();
       
        $this->logger->info("lista de Facturas.");

        return $this->respondWithData($facturas);
    }
}
