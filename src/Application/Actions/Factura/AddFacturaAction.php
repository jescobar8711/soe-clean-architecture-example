<?php
declare(strict_types=1);

namespace App\Application\Actions\Factura;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Factura\Factura;
use App\Domain\Factura\FacturaDetalle;
use App\Domain\Factura\ValueObjects\Nrofactura;
use App\Domain\Factura\ValueObjects\Fecha;
use App\Domain\Factura\ValueObjects\TotalFactura;
use App\Domain\Factura\ValueObjects\Cliente;

class AddFacturaAction extends FacturaAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
       $body = $this->request->getParsedBody(); 
       $nro =   new Nrofactura((int) $this->facturaRepository->nextNro());
       $fecha = new Fecha(\date("Y/m/d"));
        $total = new TotalFactura(floatval($body['total']));
        $cliente = new Cliente((int)$body['cliente']);
        $detalle = $body['detalle'];
        $factura=new Factura((int)$id=null, $nro, $fecha, $total, $cliente);
        $facturaId = $this->facturaRepository->addFactura($factura);
        
        foreach ($detalle as $key => $value) {
           echo $value['producto'];
            echo $value['cantidad'];
            $facturaDetalle=new FacturaDetalle((int)$id=null, (int)$facturaId, (int)$value['producto'], (int)$value['cantidad'], (float)$value['precio'], (float)$value['total']);
               $facturaDetalleId = $this->facturaDetalleRepository->addFacturaDetalle($facturaDetalle);
                $facturadetalleIds[]=$facturaDetalleId;
               
        }
        $this->logger->info("Factura id: `${facturaId}` Insertado.");
        return $this->respondWithData(array("id"=>$facturaId,"detalle"=>$facturadetalleIds));
    }
}
