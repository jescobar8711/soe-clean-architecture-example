<?php
declare(strict_types=1);

namespace App\Application\Actions\Factura;

use App\Application\Actions\Action;
use App\Domain\Factura\FacturaRepository;
use App\Domain\Factura\FacturaDetalleRepository;
use Psr\Log\LoggerInterface;

abstract class FacturaAction extends Action
{
    /**
     * @var FacturaRepository
     */
    protected $facturaRepository;
    
    /**
     * @var FacturaRepository
     */
    protected $facturaDetalleRepository;
    /**
     * @param LoggerInterface $logger
     * @param FacturaRepository  $facturaRepository
     * @param FacturaDetalleRepository  $facturaDetalleRepository
     */
    public function __construct(LoggerInterface $logger, FacturaRepository $facturaRepository, FacturaDetalleRepository $facturaDetalleRepository)
    {
   
       
        parent::__construct($logger);
        $this->facturaRepository = $facturaRepository;
        $this->facturaDetalleRepository = $facturaDetalleRepository;
    }
}
