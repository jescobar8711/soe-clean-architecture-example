<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\User\User;
use  App\Domain\User\ValueObjects\Login;
class AddUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
       $body = $this->request->getParsedBody();
      //$userId = (int)   $body['id']; 
        $login =   new Login($body['login']);
        $psw = (string)  $body['psw'];
        $email = (string)  $body['email'];
        $user=new User($userId=null,$login,$psw,$email);
        $userId = $this->userRepository->addUser($user);

        $this->logger->info("Usuario id: `${userId}` Insertado.");

        return $this->respondWithData($userId);
    }
}
