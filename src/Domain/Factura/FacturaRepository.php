<?php
declare(strict_types=1);

namespace App\Domain\Factura;

interface FacturaRepository
{
    /**
     * @return Factura[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return array
     * @throws facturaNotFoundException
     */
    public function findFacturaOfId(int $id): array;

      /**
     * @param Factura $factura
     * @return int
     * @throws FacturaNotFoundException
     */
    public function addFactura(Factura $factura): int;
    
     /**
     * Return next valid NroFactura
     *
     * @return nroFactura
     *
     * @throws FacturaNotFoundException
     */
    public function nextNro(): int;
}