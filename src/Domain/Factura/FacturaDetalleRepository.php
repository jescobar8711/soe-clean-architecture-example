<?php
declare(strict_types=1);

namespace App\Domain\Factura;

interface FacturaDetalleRepository
{
    /**
     * @return FacturaDetalle[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return array
     * @throws facturaDetalleNotFoundException
     */
    public function findFacturaDetalleOfId(int $id): array;
    /**
     * @param int $facturaId
     * @return array
     * @throws facturaDetalleNotFoundException
     */
    public function findAllOfFacturaId(int $facturaId): array;
      /**
     * @param FacturaDetalle $facturaDetalle
     * @return int
     * @throws FacturaDetalleNotFoundException
     */
    public function addFacturaDetalle(FacturaDetalle $facturaDetalle): int;
    
}