<?php
declare(strict_types=1);

namespace App\Domain\Factura;

use App\Domain\DomainException\DomainRecordNotFoundException;

class FacturaNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The user you requested does not exist.';
}
