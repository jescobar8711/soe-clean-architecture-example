<?php

namespace  App\Domain\Factura\ValueObjects;


final class Cliente
{
    /**
     * @var int
     */
    private $cliente;

    /**
     * Nrofactura constructor.
     *
     * @param int $nrofactura
     */
    public function __construct(int $cliente)
    {
        if (empty($cliente)) {
            throw new \InvalidArgumentException("cliente no puede ir vacio");
        }
        $this->cliente = $cliente;
    }

    /**
     * Return the name from the value object
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->cliente;
    }
}