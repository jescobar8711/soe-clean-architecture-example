<?php
declare(strict_types=1);

namespace App\Domain\User;

interface UserRepository
{
    /**
     * @return User[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return array
     * @throws UserNotFoundException
     */
    public function findUserOfId(int $id): array;

      /**
     * @param User $user
     * @return int
     * @throws UserNotFoundException
     */
    public function addUser(User $user): int;
    
}