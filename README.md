# soe-clean-architecture-example

aplicacion ejemplo de clean-architecture-example en PHP ejecicio practico de SOE-MDEIS-MOD06
la aplicacion usa Slim 4 con implementacion  Slim PSR-7. implementado en contenedor  PHP-DI. 

Aplicación construida para Composer.

## Pasos para instalar aplicacion

Ejecuta este comando desde el directorio que desea clonar la aplicacion
```bash
git@gitlab.com:jescobar8711/soe-clean-architecture-example.git
```
Para ejecutar la aplicación en desarrollo, se pueden ejecutar estos comandos 
```bash
cd soe-clean-architecture-example
composer start
```

o se puede usar  `docker-compose` y ejecutar tu aplicacion con `docker`, puedes ejecutar con estos comandos:
```bash
cd soe-clean-architecture-example
docker-compose up -d
```
Después de eso, habra `http://localhost:8080` en tu navegador.

La aplicacion utiliza base de datos  postgres
actualmente esta conectada a servicio PostgreSQL en  server motty.db.elephantsql.com




